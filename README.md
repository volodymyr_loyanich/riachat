# RiaChat

[![CI Status](https://img.shields.io/travis/Vladimir/RiaChat.svg?style=flat)](https://travis-ci.org/Vladimir/RiaChat)
[![Version](https://img.shields.io/cocoapods/v/RiaChat.svg?style=flat)](https://cocoapods.org/pods/RiaChat)
[![License](https://img.shields.io/cocoapods/l/RiaChat.svg?style=flat)](https://cocoapods.org/pods/RiaChat)
[![Platform](https://img.shields.io/cocoapods/p/RiaChat.svg?style=flat)](https://cocoapods.org/pods/RiaChat)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RiaChat is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'RiaChat'
```

## Author

Vladimir, volodymyr.loyanich@ria.com

## License

RiaChat is available under the MIT license. See the LICENSE file for more info.
