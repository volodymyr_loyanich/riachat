//
//  Fasad.swift
//  Alamofire
//
//  Created by Loyanich on 11/2/18.
//

import Foundation

open class Fasad {
    
    
//    public var chanelName = "private:"
//    public var chanelTopic = chanelName + userID.description
//    public var chanelParams = ["limit":"20"]
    public static var languageID = "2"
    public static var chatChannelId: Int = 0

    open static func openChatList(initChatListParams: InitChatListParams) -> UIViewController {
        
        self.languageID = initChatListParams.languageID
        return ChatListRouter.createModule(initChatListParams: initChatListParams)
//        parentViewController.navigationController?.pushViewController(vc, animated: true)
    }
    
    open static func openChat(initChatParams: InitChatParams) -> UIViewController {
        
        self.languageID = initChatParams.languageID
        return ChatRouter.createModule(initChatParams: initChatParams)
    }
}
