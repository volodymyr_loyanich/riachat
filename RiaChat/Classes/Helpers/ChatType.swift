//
//  ChatType.swift
//  RiaChat
//
//  Created by Loyanich on 11/6/18.
//

import Foundation

class ChatType {
    
    enum ID: Int { case
        
        message,
        archive,
        spam
    }
}
