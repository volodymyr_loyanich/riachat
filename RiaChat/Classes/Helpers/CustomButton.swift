//
//  Button.swift
//  ria_general
//
//  Created by Dmitriy Yavorskiy on 13.07.17.
//  Copyright © 2017 Dmitriy Yavorskiy. All rights reserved.
//

import Foundation
import UIKit

class Button: UIButton {
    
    typealias Act = ()->()
    
    private var def_act : Act?
    
    func setAction(_ action: @escaping Act) {
        self.addTarget(self, action: #selector(handleAct), for: .touchUpInside)
        self.def_act = action
    }
    
    @objc private func handleAct() {
        self.def_act!()
    }
}
