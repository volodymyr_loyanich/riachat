//
//  Assistant.swift
//  RiaChat
//
//  Created by Vladimir Loyanich on 25/11/2018.
//

import Foundation

class Assistant {
    
    static func isAssistant(ownerID: Int) -> Bool {
        
        return (ownerID == 7836975) ? true : false
//        ownerID == 7836975 ? return true : return false
    }
    
    static func getAvatarResourceName(projectID: Int) -> String {
        
        switch projectID {
        case 1:
            return "assistantAvatarRIA"
        case 2:
            return "assistantAvatarDOM"
        case 3:
            return "assistantAvatarAUTO"
        default:
            return "assistantAvatarRIA"
        }
    }
}

