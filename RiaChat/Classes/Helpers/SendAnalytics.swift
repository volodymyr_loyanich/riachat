//
//  sendAnalytics.swift
//  RiaChat
//
//  Created by Loyanich on 12/12/18.
//

import Foundation

class SendAnalytics {
    
    static func phoneCall(initChatParams: InitChatParams, userInfo: UserInfo, advertData: AdvertData, chatPlaceId: String) {
        
//        initChatParams.sendAnalytics?(AnalyticsModel(target: .flurry, category: "phone_number", action: "", label: ""))
        
        var params = [String: Any]()
        params["riaEventId"] = "215"
        params["advertId"] = advertData.id?.description
        params["advertOwnerId"] = userInfo.id?.description
        params["chatPlaceId"] = chatPlaceId
        params["projectID"] = initChatParams.projectID.description
        
        initChatParams.sendAnalytics?(AnalyticsModel(target: .slonic, category: "", action: "", label: "", params: params))
    }
}

