//
//  Loader.swift
//  AutoRia
//
//  Created by Yuriy Kudrovskiy on 08.06.16.
//  Copyright © 2016 Yuriy Kudrovskiy. All rights reserved.
//

import UIKit
import SnapKit

func loader() {
    
    DispatchQueue.main.async {
        
        Loader.one.on()
    }
}

func loaderOff() {
    
    DispatchQueue.main.async {
        Loader.one.off()
    }
}


class Loader : UIView {
    
    static let one = Loader(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
    
    var mainLoader: Loader?
    
    func on() {
        let one = Loader.one
        UIApplication.shared.delegate!.window!!.addSubview(one)
        
        one.snp.makeConstraints {
            
            $0.center.equalToSuperview()
        }
    }
    
    func onWith(_ t: String) {
        
        mainLoader = Loader.init(frame: UIApplication.shared.delegate!.window!!.frame, title: t)
        
        UIApplication.shared.delegate!.window!!.addSubview(mainLoader!)
        
        mainLoader!.snp.makeConstraints {
            $0.margins.equalToSuperview().offset(0)
        }
    }
    
    func off() {
        //AMS.p("off_main_app_loader")
        
        self.removeFromSuperview()
        self.mainLoader?.removeFromSuperview()
    }
    
    override init(frame: CGRect) {
        
//        func setAMS() {
//
//            AMS.add(self, key: "off_main_app_loader", act: { [weak self] _ in
//                self?.removeFromSuperview()
//            })
//        }
        do {
            super.init(frame: frame)
            
            let uiActInd = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
            self.backgroundColor = UIColor.clear
            //        uiActInd.center = super_view.center
            uiActInd.startAnimating()
            self.addSubview(uiActInd)
            
            uiActInd.snp.makeConstraints {
                $0.center.equalToSuperview()
            }

//            setAMS()
        }
    }
    
    // create custom loader with title
    init(frame: CGRect, title: String) {
        
        func removeCustomLoader() {
            
            self.removeFromSuperview()
        }
        
//        func setAMS() {
//            AMS.add(self, key: "off_main_app_loader", act: { [weak self] _ in
//                self?.removeFromSuperview()
//            })
//        }
        
        super.init(frame: frame)
        
        do {
            let uiActInd = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            self.backgroundColor = UIColor.clear
            uiActInd.startAnimating()
            
            let titleRect = rect(text: title,14, font_name: "SFUIDisplay-Regular")
            
            let super_view_size = UIApplication.shared.delegate!.window!!.frame.size
            
            let fr = CGRect.init(x: super_view_size.width / 2 - (titleRect.width + 52) / 2, y: super_view_size.height / 2 - 20, width: titleRect.width + 52, height: 40)
            let back_v = UIView.init(frame: fr)
            back_v.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
            
            back_v.layer.cornerRadius = 4
            back_v.layer.masksToBounds = true
            
            uiActInd.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
            back_v.addSubview(uiActInd)
            
            let lbl = UILabel.init(frame: CGRect.init(x: 40, y: 0, width: titleRect.width, height: 40))
            lbl.text = title
            lbl.font = UIFont.init(name: "SFUIDisplay-Regular", size: 14)
            lbl.textColor = UIColor.white
            back_v.addSubview(lbl)
            
            self.addSubview(back_v)
            
//            setAMS()
        }
    }
    
    private func rect(
        text: String,
        _ font_h: CGFloat,
        font_name: String = "SFUIText-Regular",
        txt_rect_w: CGFloat = CGFloat.greatestFiniteMagnitude,
        txt_rect_h: CGFloat = CGFloat.greatestFiniteMagnitude
        )-> CGRect {
        
        let xib_font = UIFont(name: font_name, size: font_h)!
        
        return calcutateRect(
            text: text,
            font: xib_font,
            width: txt_rect_w,
            h: txt_rect_h
        )
    }
    
    func calcutateRect(text:String, font:UIFont, width:CGFloat, h: CGFloat = CGFloat.greatestFiniteMagnitude) -> CGRect {
        
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: h))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
