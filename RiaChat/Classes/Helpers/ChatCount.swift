//
//  ChatCount.swift
//  RiaChat
//
//  Created by Loyanich on 11/6/18.
//

import Foundation

struct ChatCount: Codable {

    let active: Int = 0
    let archive: Int = 0
    let spam: Int = 0
}
