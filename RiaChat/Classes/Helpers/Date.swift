//
//  Date.swift
//  RiaChat
//
//  Created by Loyanich on 11/19/18.
//

import Foundation
import Localize_Swift

public extension Date {
    
    static var raw_day     : Double { return 86_400.00 }
    static var estonia_time_zone_difference : Double { return 10_800.00 } //UTC
    
    static func tryMakeDateFromDtStr(_ dateString: String) -> Date?{

        var validDateString = dateString.replacingOccurrences(of: " г.", with: "")
        validDateString = validDateString.replacingOccurrences(of: " р.", with: "")

        let date = Date.dateFormatter.date(from: validDateString)
        return date
    }
    
    static var dateFormatter : DateFormatter {
        
        let formater = DateFormatter()
        //        formater.locale = Locale(identifier: ChatLocalizableHalper.getLanguageCode())ru_UA
        formater.locale = Locale(identifier: "ru_UA")
//        formater.timeZone = TimeZone.init(identifier: "UTC")
        formater.timeZone = TimeZone.init(identifier: "UTC")
        
        //        dt_str    String "19 Ноября 2018 г."
        //        formater.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formater.dateFormat = "d MMMM yyyy"
        return formater
    }
    
    static var dateTimeFormatter : DateFormatter {
        
        let formater = DateFormatter()
        //        formater.locale = Locale(identifier: ChatLocalizableHalper.getLanguageCode())ru_UA
        formater.locale = Locale(identifier: "ru_UA")
        
//        formater.timeZone = TimeZone.init(identifier: "UTC")
        formater.timeZone = TimeZone.current
        
        //        dt_str    String    "19 Ноября 2018 г."
        //        formater.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formater.dateFormat = "HH:mm"
        return formater
    }
    
    static var curentTimestamp : Double {
        
        return Date().timeIntervalSince1970
    }
    
    static func makeDateFromTS(_ ts : Int) -> Date {
        return Date(timeIntervalSince1970: TimeInterval(ts))
    }
    
    static var curentDate : Date {
        
        let date = Date.makeDateFromTS(Int(curentTimestamp))
        return date
    }
    
    static var timestamp_from_day_start: Double {
        
        let cal = Calendar.current
        
        let cal_components = (cal as NSCalendar).components([.hour, .minute, .second], from: Date())
        return Double(cal_components.hour!) * 60.00 * 60.00 + Double(cal_components.minute!) * 60.00 + Double(cal_components.second!)
    }
//    static func makeStr(_ date: Date? = nil, style : DateStrFormat, is_future : Bool = false) -> String {
    static func makeCustomString(_ dateString: String) -> String {
        
        
        if let date = tryMakeDateFromDtStr(dateString) {
            
            let incoming_ts = date.timeIntervalSince1970
            
            let cur_ts = Date().timeIntervalSince1970
            
            let difrence = cur_ts - incoming_ts
            
            let diference_date = Date(timeIntervalSince1970: difrence)
            
            switch difrence {
            case difrence where difrence < timestamp_from_day_start:
                
                return localizedString(forKey: "today")
                
            case difrence where difrence < (raw_day + timestamp_from_day_start):

                return localizedString(forKey: "yesterday")
                
            default:
                return dateString
            }
        }
        
        return dateString
    }

}
