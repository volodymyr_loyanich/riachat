//
//  DialogStatus.swift
//  RiaChat
//
//  Created by Loyanich on 11/8/18.
//

import Foundation

public enum DialogStatusID: Int {
    case message,
    archive,
    spam
}

