//
//  ChatLocalizableHalper.swift
//  RiaChat
//
//  Created by Loyanich on 11/2/18.
//

import Foundation
//import Localize_Swift

class ChatLocalizableHalper {
    
    static func getLanguageCode() -> String {
        
        var languageCode = "en"
        if Fasad.languageID == "2" {
            
            languageCode = "ru"
        } else if Fasad.languageID == "4" {
            
            languageCode = "uk"
        }
        
        return languageCode
    }
}

func localizedString(forKey key: String) -> String {
    
    let localizablePath = Bundle(for: ChatLocalizableHalper.self).resourcePath!
    let localizableBundle = Bundle(path: localizablePath)!
    
    var result = key
    var languagePathName = ChatLocalizableHalper.getLanguageCode()
    
    if let languagePath = localizableBundle.path(forResource: languagePathName, ofType: "lproj"),
        let languageBundle = Bundle(path: languagePath) {
        
        result = languageBundle.localizedString(forKey: key, value: nil, table: "ChatLocalizable")
    }

    return result
}
