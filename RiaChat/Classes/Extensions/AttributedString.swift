//
//  AttributedString.swift
//  DOMRIA
//
//  Created by Dmitriy Yavorskiy on 01.06.2018.
//  Copyright © 2018 INTERNATIONAL ONLINE TRANSACTIONS OÜ. All rights reserved.
//

import UIKit

public extension NSMutableAttributedString {
    
    static func setText(_ attributes: [(text: String, color: UIColor, font: UIFont)],
                 lineSpace: CGFloat = 4,
                 aligment: NSTextAlignment = .left,
                 lineBreakMode: NSLineBreakMode = .byWordWrapping) -> NSMutableAttributedString {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = aligment
        paragraphStyle.lineSpacing = lineSpace
        paragraphStyle.lineBreakMode = lineBreakMode
        
        let attributedString = NSMutableAttributedString.init()
        
        for item in attributes {
            
            let font = item.font
            
            let attrsibut = [NSAttributedStringKey.font : font, NSAttributedStringKey.foregroundColor : item.color]
            
            let attrString = NSMutableAttributedString(string: item.text, attributes: attrsibut)
            
            attributedString.append(attrString)
        }
        
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        
        return attributedString
    }
    
    static func setTxtLineSpace(_ txt: String?, lineSpace: CGFloat) -> NSMutableAttributedString {
        
        let paragraphStyle = NSMutableParagraphStyle()
        
        paragraphStyle.lineSpacing = lineSpace
        
        let atr_str = NSMutableAttributedString(string: txt ?? "")
        
        atr_str.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, atr_str.length))
        
        return atr_str
    }
}

extension NSAttributedString {
    
    class func tryMakeFrom(_ html_str : String) -> NSAttributedString? {
        
        if let html_txt_data = html_str.data(using: String.Encoding.unicode, allowLossyConversion: false) {
            
            
            return try? NSAttributedString(data: html_txt_data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        return nil
    }
    
    func makeHeight(_ w : CGFloat) -> CGFloat {
        return self.boundingRect(with: CGSize(width: w, height: CGFloat.greatestFiniteMagnitude), options: [NSStringDrawingOptions.usesLineFragmentOrigin, NSStringDrawingOptions.usesFontLeading], context: nil).height
    }
}

extension NSMutableAttributedString {
    
    /**
     #DEPRECATED: use new "setText"!!!
     */
    static func setTxt(_ part1: String?, color1: UIColor, font1: UIFont?,
                       part2: String?, color2: UIColor, font2: UIFont?,
                       part3: String?=nil, color3: UIColor?=nil, font3: UIFont?=nil,
                       part4: String?=nil, color4: UIColor?=nil, font4: UIFont?=nil, lineSpace: CGFloat=4) -> NSMutableAttributedString {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpace
        
        let attrs1 = [NSAttributedStringKey.font : font1 ?? UIFont.systemFont(ofSize: 20), NSAttributedStringKey.foregroundColor : color1]
        
        let attributedString1 = NSMutableAttributedString(string: part1 ?? "", attributes:attrs1)
        
        if let part2 = part2,
            let font2 = font2 {
            
            let attrs2 = [NSAttributedStringKey.font : font2, NSAttributedStringKey.foregroundColor : color2]
            
            let attributedString2 = NSMutableAttributedString(string: part2, attributes:attrs2)
            
            attributedString1.append(attributedString2)
        }
        
        if let part3 = part3,
            let color3 = color3,
            let font3 = font3 {
            
            let attrs3 = [NSAttributedStringKey.font : font3, NSAttributedStringKey.foregroundColor : color3]
            
            let attributedString3 = NSMutableAttributedString(string: part3, attributes:attrs3)
            
            attributedString1.append(attributedString3)
        }
        
        if let part4 = part4,
            let color4 = color4,
            let font4 = font4 {
            
            let attrs4 = [NSAttributedStringKey.font : font4, NSAttributedStringKey.foregroundColor : color4]
            
            let attributedString4 = NSMutableAttributedString(string: part4, attributes:attrs4)
            
            attributedString1.append(attributedString4)
        }
        
        attributedString1.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString1.length))
        
        return attributedString1
    }
}

extension NSAttributedString {
    
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.width)
    }
}
