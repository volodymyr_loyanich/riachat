//
//  Color.swift
//  ria_general
//
//  Created by Dmitriy Yavorskiy on 29.06.17.
//  Copyright © 2017 Dmitriy Yavorskiy. All rights reserved.
//

import Foundation
import UIKit

public extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hex: Int) {
        self.init(
            red: (hex >> 16) & 0xFF,
            green: (hex >> 8) & 0xFF,
            blue: hex & 0xFF
        )
    }
}

/**
 
 Attansion!
 
 Use only default format:
 
 .init(hex: 0x404040)
 
 not:
 
 .init(red: 0.02, green: 0.23, blue: 0.34, alpha: 1.00)
 
 not:
 
 .init(red: 217/255, green: 92/255, blue: 76/255, alpha: 1)
 
 */
public extension UIColor {
    
    class var ria_gray_EEF2F3: UIColor {
        
        return .init(hex: 0xEEF2F3)
    }
    
    class var ria_orange_F49310: UIColor {
        
        return .init(hex: 0xF49310)
    }
    
    class var ria_orange_light: UIColor {
        
        return .init(hex: 0xFDF5D9)
    }
    
    class var ria_brand_darck_blue: UIColor {
        
        return .init(hex: 0x053a56)
    }
    
    class var ria_brand_green: UIColor {
        
        return .init(hex: 0x7abc23)
    }
    
    class var ria_brand_gray: UIColor {
        
        return .init(hex: 0xa3aaaf)
    }
    
    class var ria_brand_light: UIColor {
        
        return .init(hex: 0xf4f7f9)
    }
    
    class var ria_brand_color_pale_red: UIColor {
        
        return .init(hex: 0xd95c4c)
    }
    
    class var ria_brand_color_gray_240: UIColor {
        
        return .init(hex: 0xf0f0f0)
    }
    
    class var ria_brand_color_gray_128: UIColor {
        
        return .init(hex: 0x808080)
    }
    
    class var ria_brand_color_gray_137: UIColor {
        
        return .init(hex: 0x898989)
    }
    
    class var ria_brand_color_light_black: UIColor {
        
        return .init(hex: 0x404040)
    }
    
    class var ria_form_text_gray_65: UIColor {
        
        return .init(hex: 0x414141)
    }
    
    class var ria_form_border_gray: UIColor {
        
        return .init(hex: 0xdfe3e4)
    }
    
    class var ria_form_text_gray_disable: UIColor {
        
        return .init(hex: 0x9ca6aa)
    }
    
    class var ria_dark_blue_main: UIColor {
        
        return .init(hex: 0x296897)
    }
    
    class var ria_green_main: UIColor {
        
        return .init(hex: 0x5bc120)
    }
    
    class var ria_darck_green: UIColor {
        
        return .init(hex: 0x3c9806)
    }
    
    class var ria_green_79BE00: UIColor {
        
        return .init(hex: 0x79BE00)
    }
    
    class var ria_light_black: UIColor {
        
        return .init(hex: 0x404042)
    }
    
    class var ria_darck_blue: UIColor {
        
        return .init(hex: 0x24679b)
    }
    
    class var ria_light_cyan: UIColor {
        
        return .init(hex: 0xecf7fd)
    }
    
    class var ria_cyan: UIColor {
        
        return .init(hex: 0x219be8)
    }
    
    class var ria_light_yellow: UIColor {
        
        return .init(hex: 0xf0fbdb)
    }
    
    class var ria_light_gray: UIColor {
        
        return .init(hex: 0xf6f7f9)
    }
    
    class var ria_light_gray_e6: UIColor {
        
        return .init(hex: 0xe6e6e6)
    }
    
    class var ria_light_gray_9da6aa: UIColor {
        
        return .init(hex: 0x9da6aa)
    }
    
    class var ria_black_414042: UIColor {
        
        return .init(hex: 0x414042)
    }
    
    class var ria_red_FF4202: UIColor {
        
        return .init(hex: 0xFF4202)
    }
    
    class var ria_red_FF3333: UIColor {
        
        return .init(hex: 0xFF3333)
    }
    
    class var ria_darck_green_669900: UIColor {
        
        return .init(hex: 0x669900)
    }
    
    class var ria_gray_9B9B9B: UIColor {
        
        return .init(hex: 0x9B9B9B)
    }
    
    class var ria_blue_256799: UIColor {
        
        return .init(hex: 0x256799)
    }
    
    class var ria_blue_219BE7: UIColor {
        
        return .init(hex: 0x219BE7)
    }
    
    class var ria_orange_EF9113: UIColor {
        
        return .init(hex: 0xEF9113)
    }
    
    class var ria_gray_9F9F9F: UIColor {
        
        return .init(hex: 0x9F9F9F)
    }
    
    class var ria_gray_F0F2FA: UIColor {
        
        return .init(hex: 0xF0F2FA)
    }
    
    class var ria_gray_E0E3E4: UIColor {
        
        return .init(hex: 0xE0E3E4)
    }
    
    class var ria_blue_003B56: UIColor {
        
        return .init(hex: 0x003B56)
    }
    
    class var ria_gray_E5E5E5: UIColor {
        
        return .init(hex: 0xE5E5E5)
    }
    
    class var ria_F09213: UIColor {
        
        return .init(hex: 0xF09213)
    }
    
    class var ria_e3f2fc: UIColor {
        
        return .init(hex: 0xE3F2FC)
    }
    
    class var ria_535355: UIColor {
        
        return .init(hex: 0x535355)
    }
    
    class var ria_045E96: UIColor {
        
        return .init(hex: 0x045E96)
    }
    
    class var ria_F0FADC: UIColor {
        
        return .init(hex: 0xF0FADC)
    }
    
    class var ria_79BD00: UIColor {
        
        return .init(hex: 0x79BD00)
    }
    
    class var ria_image_stroke: UIColor {
        
        return .init(hex: 0xE0E3E4)
    }
    
    class var ria_phone_namber: UIColor {

        return .init(hex: 0x669900)
    }
    
    class var ria_empty_chat_background: UIColor {

        return .init(hex: 0xF0F2FA)
    }
}

// from auto ria

extension UIColor {
    
    // MARK:
    // MARK: Macro
    // MARK: colors
    var ria_brand_color_auto_main       : UIColor {  return UIColor().ria_brand_color_paleRed}
    var ria_brand_color_dom_main        : UIColor {  return  UIColor(red: 255/255, green: 121/255, blue: 0/255, alpha: 1)}
    var ria_brand_color_ria_main        : UIColor {  return  UIColor(red: 0/255, green: 110/255, blue: 156/255, alpha: 1)}
    var ria_brand_color_black           : UIColor {  return UIColor(red: 26/255, green: 26/255, blue: 26/255)}
    var ria_brand_color_paleRed         : UIColor {  return UIColor(red: 217/255, green: 92/255, blue: 76/255, alpha: 1)}
    var ria_brand_color_darkTeal        : UIColor {  return UIColor(red: 0/255, green: 59/255, blue: 85/255, alpha: 1)}
    var ria_brand_color_brownishGrey    : UIColor {  return UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1)}
    var ria_brand_color_warmGrey        : UIColor {  return UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)}
    var ria_brand_color_white1          : UIColor {  return UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1)} // light gray
    var ria_brand_color_reddish         : UIColor {  return UIColor(red: 194/255, green: 69/255, blue: 55/255, alpha: 1)}
    var ria_brand_color_orangeyYellow   : UIColor {  return UIColor(red: 255/255, green: 190/255, blue: 25/255, alpha: 1)}
    var ria_brand_color_white           : UIColor {  return UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)}
    var ria_brand_color_darkYellowGreen : UIColor {  return UIColor(red: 102/255, green: 153/255, blue: 0/255, alpha: 1)}
    var ria_brand_color_green_main      : UIColor {  return UIColor(red: 114/255, green: 171/255, blue: 0/255, alpha: 1)}
    var ria_brand_color_separator_1     : UIColor {  return UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1)}
    var ria_brand_color_background_1    : UIColor {  return UIColor(red: 237/255, green: 237/255, blue: 237/255, alpha: 1)}
    var ria_brand_color_blue_1          : UIColor {  return UIColor(red: 17/255, green: 153/255, blue: 234/255, alpha: 1)}
    var ria_brand_color_blue_2          : UIColor {  return UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)}
    var ria_brand_color_gray            : UIColor {  return UIColor(red: 0.62, green: 0.65, blue: 0.67, alpha: 1)}
    var ria_brand_color_background_list_light: UIColor {  return UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)}
    var ria_brand_color_transparent_black_30: UIColor {  return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.30)}
    var ria_brand_color_red: UIColor {  return UIColor(red: 206/255, green: 70/255, blue: 60/255, alpha: 1)}
}
