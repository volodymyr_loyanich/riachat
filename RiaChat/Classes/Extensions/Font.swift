//
//  Font.swift
//  DOM.RIA
//
//  Created by Dmitriy Yavorskiy on 25.05.2018.
//  Copyright © 2018 INTERNATIONAL ONLINE TRANSACTIONS OÜ. All rights reserved.
//

import UIKit

extension UIFont {
    
    struct Arial {
        
        static func regular(_ size: CGFloat = 16) -> UIFont {
            
            return UIFont(name: "ArialMT", size: size) ?? UIFont.systemFont(ofSize: size)
        }
        
        static func bold(_ size: CGFloat = 16) -> UIFont {
            
            return UIFont(name: "Arial-BoldMT", size: size) ?? UIFont.boldSystemFont(ofSize: size)
        }
        
        static func italic(_ size: CGFloat = 16) -> UIFont {
            
            return UIFont(name: "Arial-ItalicMT", size: size) ?? UIFont.italicSystemFont(ofSize: size)
        }
        
        static func boldItalic(_ size: CGFloat = 16) -> UIFont? {
            
            return UIFont(name: "Arial-BoldItalicMT", size: size)
        }
    }
}
