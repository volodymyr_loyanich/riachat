//
//  NaviParamsExchange.swift
//  RiaChat
//
//  Created by Loyanich on 11/26/18.
//

import Foundation

protocol NaviParamsExchange {
    var incoming_params: [String : Any]? {get set}
}
