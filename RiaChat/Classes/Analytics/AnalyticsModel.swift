//
//  AnalyticsModel.swift
//  RiaChat
//
//  Created by Loyanich on 12/3/18.
//

import Foundation
//GAIHelper.event([
//    .category("subscriptions"),
//    .action("button_press"),
//    .label("search_result_subs")
//    ])

open class AnalyticsModel {
    
    public enum AnalyticsTarget: Int { case
        
        flurry,
        googleAnalytics,
        slonic
    }
    
    public let target: AnalyticsTarget
    public let category: String?
    public let action: String?
    public let label: String?
    public let params: [String: Any]
    
    public init(target: AnalyticsTarget, category: String?, action: String?, label: String?, params: [String: Any] = [String: Any]()) {
        
        self.target = target
        self.category = category
        self.action = action
        self.label = label
        self.params = params
    }
}
