//
//  ChatSocket.swift
//  Alamofire
//
//  Created by Loyanich on 11/1/18.
//

import Foundation
import SwiftPhoenixClient
import RxSwift

enum ChatEvents : String {
    case nextPageTypeRes        = "next_page_type_res"
    case initDataMessage        = "init_data_msg"
    case upChatCounters         = "up_chat_types_counters"
    case nextPage               = "next_page"
    case nextPageType           = "next_page_type"
    case sendStatus             = "send_status"
    case setMessageList         = "set_message_list"
    case setOpponent            = "set_opponent"
    case setOpponentStatus      = "set_opponent_status"
    case setAdvInfo             = "set_adv_info"
    case newMessage             = "new_message"
    case nextPageRes            = "next_page_res"
    case initDataByType         = "init_data_by_type" //"status":"0" 0 - активні, 1 - архів, 2 - спам
    case initDataMsgByType      = "init_data_msg_by_type"
    case toStatus               = "to_status"
    case newChatMsg             = "new_chat_msg"
    case createChatMsg          = "create_chat_msg"
    case setStatus              = "set_status"
    case setMessage             = "set_message"
    case addMessage             = "add_message"
    case remainingMessageList   = "remaining_message_list"
    case updateUnreadCounter    = "update_unread_counter"
    case setOnlineStatus        = "set_online_status"
    case getLeftMessages        = "get_left_messages"
}

class ChatSocket {

    let socket: Socket
    var channelChatList: Channel?
    var channelChat: Channel?
    var isChannelChatListReady = false
    var isChannelChatReady = false
    let disposeBag = DisposeBag()
    var page = 1

    init(initChatListParams: InitChatListParams?, initChatParams: InitChatParams?) {
        
        var pspID = ""
        if let pspIDChatList = initChatListParams?.pspID {
            
            pspID = pspIDChatList
        }
        if let pspIDChat = initChatParams?.pspID {
            
            pspID = pspIDChat
        }
        
        var languageID = ""
        if let languageIDChatList = initChatListParams?.languageID {

            languageID = languageIDChatList == "2" ? "ru" : "uk"
        }
        if let languageIDChat = initChatParams?.languageID {
            
            languageID = languageIDChat == "2" ? "ru" : "uk"
        }
        socket = Socket(url: "wss://chat.ria.com/socket/websocket", params: ["locale": languageID, "vsn": "1.0.0", "PSP_ID": pspID])
//        socket = Socket(url: "wss://chat.ria.com/socket/websocket", params: ["locale": "ru", "vsn": "1.0.0", "PSP_ID": "2edf438f34681947c2df4650f3863651dc6f43b8ea30d07cc883963f737dc939929064"])
    }
    
    func observSocketOpened () -> Observable<Bool> {
        
        return Observable.create {[weak self] observer -> Disposable in
            
            self?.socket.onOpen {
                
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }
    
    func observSocketError () -> Observable<Bool> {
        
        return Observable.create {[weak self] observer -> Disposable in
            
            self?.socket.onError { [weak self] error in
                
                self?.socketClose()
//                self?.socket.disconnect()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
                    observer.onNext(false)
                })
                
                print("Socket has errored: ", error.localizedDescription)
            }
            return Disposables.create()
        }
    }
    
    func observSocketErrorNetworkConnections () -> Observable<Bool> {
        
        return Observable.create {[weak self] observer -> Disposable in
            
            self?.socket.logger = { msg in
                
                if msg.contains("Network is down") || msg.contains("Code=50") {
                    
                    observer.onNext(false)
                }
                print(msg)
            }

            return Disposables.create()
        }
    }
    
    func connectToSocket() {

//        socket.onOpen {
//
//            callback()
//        }
        
        socket.onClose { [weak self] in

            self?.socketClose()
        }
        
//        socket.onError { error in
//            self.socket.disconnect()
//
//            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10), execute: {
//                self.socket.connect()
//            })
//
//            print("Socket has errored: ", error.localizedDescription)
//        }
        
        socket.logger = { msg in

            print(msg)
        }

        socket.connect()
    }
    
    func initChanelChatList(initChatListParams: InitChatListParams) -> Observable<Bool> {
        
        let params = ["limit":"20"]
        
        let chanelTopic = "private:" + initChatListParams.userID.description
        channelChatList = self.socket.channel(chanelTopic, params: params)
       
        return Observable.create {[weak self] observer -> Disposable in
            
            if let self_ = self,
                let channelChatList = self_.channelChatList {
                
                channelChatList
                    .join()
                    .receive("ok") {[weak self] (payload) in
                        
                        if let self_ = self {
                            
                            self_.isChannelChatListReady = true
                            //                        observer.onNext(true)
                            
                        }
                        observer.onCompleted()
                        //                        observer.onCompleted()
                    }.receive("error") { (payload) in

                        self_.isChannelChatListReady = false
                    }
            }
            return Disposables.create()
        }
    }
    
    func initChanelChat(initChatParams: InitChatParams) -> Observable<Bool> {
        
        let chanelTopic = "chat:" + initChatParams.chatID.description
        let params = ["messages_limit": 20, "adv_id": initChatParams.advertID]
        
        channelChat = self.socket.channel(chanelTopic, params: params)
        
        return Observable.create {[weak self] observer -> Disposable in
            
            if let self_ = self,
                let channelChat = self_.channelChat {
                
                channelChat
                    .join()
                    .receive("ok") {[weak self] (payload) in
                        
//                        let response = payload.payload["response"]
//                        let status = payload.payload["status"]
//                        print("error: ", payload.payload)
//                        print("error: ", payload.payload["response"])
                        
                        if let self_ = self {
                            
                            self_.isChannelChatReady = true
                            //                        observer.onNext(true)
                            
                        }
                        observer.onCompleted()
                        //                        observer.onCompleted()
                    }.receive("error") { (payload) in
                        
                        self_.isChannelChatReady = false
                }
            }
            return Disposables.create()
        }
    }
    
    func requestInitChatList(chatType: ChatType.ID) {
        
        self.page = 1
        
        if isChannelChatListReady {
            
            channelChatList?
                .push(ChatEvents.initDataByType.rawValue, payload: ["status": chatType.rawValue.description])
                .receive("ok") { (message) in
                    print("success", message)
                }
                .receive("error") { (errorMessage) in
                    print("error: ", errorMessage)
            }
        }
    }
    
    func requestCreateNewChat(initChatParams: InitChatParams) {
        
        var payloadParam = ["adv_id": initChatParams.advertID,
                            "project_id": initChatParams.projectID,
                            "secure": initChatParams.secureKey] as [String : Any]
        
        if isChannelChatListReady {
            
            channelChatList?
                .push(ChatEvents.createChatMsg.rawValue, payload: payloadParam)
                .receive("ok") { (message) in
                    print("success", message)
                }
                .receive("error") { (errorMessage) in
                    print("error: ", errorMessage)
            }
        }
    }
    
    func requestInitChat() {
        
        if isChannelChatReady {
            
            channelChat?
                .push(ChatEvents.getLeftMessages.rawValue, payload: [:])
                .receive("ok") { (message) in
                    print("success", message)
                }
                .receive("error") { (errorMessage) in
                    //                let response = errorMessage.payload["response"]
                    //                let status = errorMessage.payload["status"]
                    //                print("error: ", errorMessage.payload)
                    //                print("error: ", errorMessage.payload["response"])
            }
        }
    }
    
    func requestNextChatList(chatType: ChatType.ID) {
        
        var chatEvents = ChatEvents.nextPageType.rawValue

        self.page += 1
        var payloadParam = ["status":"OK", "page": self.page, "type": chatType.rawValue] as [String : Any]

        
        if chatType.rawValue == ChatType.ID.message.rawValue {
            
            chatEvents = ChatEvents.nextPage.rawValue
            payloadParam = ["status":"ok"]
        }
        
        if isChannelChatListReady {
            
            channelChatList?
                .push(chatEvents, payload: payloadParam)
                .receive("ok") { (message) in
                    
                }
                .receive("error") { (errorMessage) in
                    print("error: ", errorMessage)
            }
        }
    }
    
    func sendMessage(chatModel: ChatModel) -> Observable<ChatModel> {

        return Observable.create {[weak self] observer -> Disposable in
            
            let payloadParam = ["author_id": chatModel.authorID,
                                "id": 0,
                                "status": "OK",
                                "time": "",
                                "date": "",
                                "text": chatModel.text] as [String : Any]
            
            if let isChannelReady = self?.isChannelChatReady,  isChannelReady {
                
                self?.channelChat?
                    .push(ChatEvents.newMessage.rawValue, payload: payloadParam)
                    .receive("ok") { (message) in
                        
                        observer.onNext(chatModel)
                        observer.onCompleted()
                    }
                    .receive("error") { (errorMessage) in
                        
                        observer.onError(errorMessage as! Error)
                }
            }

            return Disposables.create()
        }
    }
    
    func requestNextChatMessages() {
        
//        channel.push("get_left_messages", {});

    }
    
    func changeDialogStatus(chatID: Int, newStatus: ChatType.ID) {

        let payloadParam = ["chat_id": chatID, "status": newStatus.rawValue] as [String : Any]
        
        if isChannelChatListReady {
            
            channelChatList?
                .push(ChatEvents.toStatus.rawValue, payload: payloadParam)
                .receive("ok") { (message) in
                    
                }
                .receive("error") { (errorMessage) in
                    print("error: ", errorMessage)
            }
        }
    }
    
    func setReadStatus(chatID: Int, userID: Int) {
        
        let payloadParam: [String : Any] = ["chat_id": chatID, "status": "readed", "user_id": userID]
        
        if isChannelChatReady {
            
            channelChat?
                .push(ChatEvents.setStatus.rawValue, payload: payloadParam)
                .receive("ok") { (message) in
                    print(message)
                }
                .receive("error") { (errorMessage) in
                    print("error: ", errorMessage)
                }.receive("timeout") { payload in
                    print("Networking issue...", payload)
                    
            }
        }
    }
    
    private func chatListObservable(chatEvents: ChatEvents) -> Observable<[ChatListItem]> {
        
        return Observable.create {[weak self] observer -> Disposable in
            
            if let self_ = self {
                
                self_.channelChatList?.on(chatEvents.rawValue) { (payload) in
                    
                    if let jsonA = payload.payload["chats"] as? [String: [Any]] {
                        
                        print(jsonA)
                    }
                    
                    do {
                        if let json = payload.payload["chats"],
                            JSONSerialization.isValidJSONObject(json),
                            let jsonData = try? JSONSerialization.data(withJSONObject: json) {
                            
                            let myStruct = try JSONDecoder().decode([ChatListItem].self, from: jsonData) // Decoding our data
                            observer.onNext(myStruct)
                            //                                observer.onCompleted()
                            //                        self.presenter?.createModelFromSource(chatListItem: myStruct)
                        }
                        
                    } catch let error as NSError {

                        observer.onError(error)
                    }
                }
                
            }
            return Disposables.create()
        }
    }
    
    func subscribeOnResponseInitChatList() -> Observable<[ChatListItem]> {
        
        return chatListObservable(chatEvents: ChatEvents.initDataMsgByType)
    }
    
    func subscribeOnResponseActiveChatList() -> Observable<[ChatListItem]> {

        return chatListObservable(chatEvents: ChatEvents.nextPageRes)
    }
    
    func subscribeOnResponseTypeChatList() -> Observable<[ChatListItem]> {
        
        return chatListObservable(chatEvents: ChatEvents.nextPageTypeRes)
    }
    
    func subscribeOnChatListCounts() -> Observable<ChatListCounters> {
        return Observable.create {[weak self] observer -> Disposable in
            
            if let self_ = self {
                
                self_.channelChatList?.on(ChatEvents.upChatCounters.rawValue) { (payload) in
                    
                    do {
                        if let json = payload.payload["chat_types_counters"],
                            JSONSerialization.isValidJSONObject(json),
                            let jsonData = try? JSONSerialization.data(withJSONObject: json) {
                            let chatCount = try JSONDecoder().decode(ChatListCounters.self, from: jsonData) // Decoding our data

                            observer.onNext(chatCount)
//                            observer.onCompleted()
                            //                        self.presenter?.createModelFromSource(chatListItem: myStruct)
                        }

                    } catch let error as NSError {
                        
                        observer.onError(error)
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func subscribeOnChatListNewMessage() -> Observable<ChatListNewMessage> {
        
        return Observable.create {[weak self] observer -> Disposable in
            
            if let self_ = self {
                print("payload", ChatEvents.addMessage.rawValue)
                self_.channelChatList?.on(ChatEvents.addMessage.rawValue) { (payload) in
                    
                    print("payload", payload)

                    do {
                        if JSONSerialization.isValidJSONObject(payload.payload),
                            let jsonData = try? JSONSerialization.data(withJSONObject: payload.payload) {
                            let newMessage = try JSONDecoder().decode(ChatListNewMessage.self, from: jsonData) // Decoding our data

                            observer.onNext(newMessage)
                        }
                        
                    } catch let error as NSError {
//                        print("error", error.localizedDescription)
                        observer.onError(error)
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func subscribeOnNewChatCreated() -> Observable<ChatListItem> {
        
        return Observable.create {[weak self] observer -> Disposable in
            
            if let self_ = self {
                
                self_.channelChatList?.on(ChatEvents.newChatMsg.rawValue) { (payload) in
                    
                    print("payload", payload)
                    
                    do {
                        if JSONSerialization.isValidJSONObject(payload.payload),
                            let jsonData = try? JSONSerialization.data(withJSONObject: payload.payload) {
                            let chat = try JSONDecoder().decode(NewChat.self, from: jsonData) // Decoding our data
                            
                            if let newChat = chat.chat {
                               
                                observer.onNext(newChat)
                            }
                            
                        }
                        
                    } catch let error as NSError {
                        //                        print("error", error.localizedDescription)
                        observer.onError(error)
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func subscribeOnChatMessage() -> Observable<ChatMessages> {
        
        return Observable.create {[weak self] observer -> Disposable in
            
            if let self_ = self {

                self_.channelChat?.on(ChatEvents.setMessageList.rawValue) { (payload) in
                    
                    do {
                        if JSONSerialization.isValidJSONObject(payload.payload),
                            let jsonData = try? JSONSerialization.data(withJSONObject: payload.payload) {
                            let messages = try JSONDecoder().decode(ChatMessages.self, from: jsonData) // Decoding our data
                            print("messages", messages)
                            print("messages", payload.payload)
                            observer.onNext(messages)
                        }
                        
                    } catch let error as NSError {
                        //                        print("error", error.localizedDescription)
                        observer.onError(error)
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func subscribeOnNextChatMessage() -> Observable<ChatMessages> {
        
        return Observable.create {[weak self] observer -> Disposable in
            
            if let self_ = self {
                
                self_.channelChat?.on(ChatEvents.remainingMessageList.rawValue) { (payload) in
                    
                    do {
                        if JSONSerialization.isValidJSONObject(payload.payload),
                            let jsonData = try? JSONSerialization.data(withJSONObject: payload.payload) {
                            let messages = try JSONDecoder().decode(ChatMessages.self, from: jsonData) // Decoding our data
                            print("messages", messages)
                            observer.onNext(messages)
                        }
                        
                    } catch let error as NSError {
                        //                        print("error", error.localizedDescription)
                        observer.onError(error)
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func subscribeOnNewChatMessage() -> Observable<ChatMessage> {
        
        return Observable.create {[weak self] observer -> Disposable in
            
            if let self_ = self {
                
                self_.channelChat?.on(ChatEvents.setMessage.rawValue) { (payload) in
                    
                    do {
                        if JSONSerialization.isValidJSONObject(payload.payload),
                            let jsonData = try? JSONSerialization.data(withJSONObject: payload.payload) {
                            let messages = try JSONDecoder().decode(ChatListNewMessage.self, from: jsonData) // Decoding our data
                            
                            if let message = messages.message {

                                observer.onNext(message)
                            }
                        }
                        
                    } catch let error as NSError {
                        //                        print("error", error.localizedDescription)
                        observer.onError(error)
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func subscribeOnOpponentStatus() -> Observable<UserStatus> {
        
        return Observable.create {[weak self] observer -> Disposable in
            
            if let self_ = self {
                
                self_.channelChat?.on(ChatEvents.setOpponentStatus.rawValue) { (payload) in
                    
                    do {
                        if JSONSerialization.isValidJSONObject(payload.payload),
                            let jsonData = try? JSONSerialization.data(withJSONObject: payload.payload) {
                            let userStatus = try JSONDecoder().decode(UserStatus.self, from: jsonData) // Decoding our data
                            
                            observer.onNext(userStatus)
                        }
                        
                    } catch let error as NSError {
                        //                        print("error", error.localizedDescription)
                        observer.onError(error)
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func subscribeOnUserInfo() -> Observable<UserInfo> {
        
        return Observable.create {[weak self] observer -> Disposable in
            
            if let self_ = self {
                
                self_.channelChat?.on(ChatEvents.setOpponent.rawValue) { (payload) in
                    
                    do {
                        if JSONSerialization.isValidJSONObject(payload.payload),
                            let jsonData = try? JSONSerialization.data(withJSONObject: payload.payload) {
                            let userInfo = try JSONDecoder().decode(UserInfo.self, from: jsonData) // Decoding our data
                            
                            observer.onNext(userInfo)
                        }
                        
                    } catch let error as NSError {
                        //                        print("error", error.localizedDescription)
                        observer.onError(error)
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func subscribeOnAdvertInfo() -> Observable<AdvertInfo> {
        return Observable.create {[weak self] observer -> Disposable in
            
            if let self_ = self {
                
                self_.channelChat?.on(ChatEvents.setAdvInfo.rawValue) { (payload) in
                    
                    do {
                        if JSONSerialization.isValidJSONObject(payload.payload),
                            let jsonData = try? JSONSerialization.data(withJSONObject: payload.payload) {
                            let advertInfo = try JSONDecoder().decode(AdvertInfo.self, from: jsonData) // Decoding our data
                            
                            observer.onNext(advertInfo)
                        }
                        
                    } catch let error as NSError {
                        //                        print("error", error.localizedDescription)
                        observer.onError(error)
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func subscribeOnUpdateUnreadCounter() -> Observable<UnreadCounter> {
        return Observable.create {[weak self] observer -> Disposable in
            
            if let self_ = self {
                
                self_.channelChat?.on(ChatEvents.updateUnreadCounter.rawValue) { (payload) in
                    
                    do {
                        if JSONSerialization.isValidJSONObject(payload.payload),
                            let jsonData = try? JSONSerialization.data(withJSONObject: payload.payload) {
                            let unreadCounter = try JSONDecoder().decode(UnreadCounter.self, from: jsonData) // Decoding our data
                            
                            observer.onNext(unreadCounter)
                        }
                        
                    } catch let error as NSError {
                        //                        print("error", error.localizedDescription)
                        observer.onError(error)
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func subscribeOnUpdateListUnreadCounter() -> Observable<UnreadCounter> {
        return Observable.create {[weak self] observer -> Disposable in
            
            if let self_ = self {
                
                self_.channelChatList?.on(ChatEvents.updateUnreadCounter.rawValue) { (payload) in
                    
                    do {
                        if JSONSerialization.isValidJSONObject(payload.payload),
                            let jsonData = try? JSONSerialization.data(withJSONObject: payload.payload) {
                            let unreadCounter = try JSONDecoder().decode(UnreadCounter.self, from: jsonData) // Decoding our data
                            
                            observer.onNext(unreadCounter)
                        }
                        
                    } catch let error as NSError {
                        //                        print("error", error.localizedDescription)
                        observer.onError(error)
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func socketClose() {
        
        channelChatList?.leave()
        channelChat?.leave()
        isChannelChatReady = false
        isChannelChatListReady = false
        socket.removeAllCallbacks()
        socket.disconnect()
    }
    
    deinit {
        
        print("deinit ChatSocket")
        socketClose()
    }
}
