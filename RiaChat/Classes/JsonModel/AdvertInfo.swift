//
//  AdvertInfo.swift
//  RiaChat
//
//  Created by Loyanich on 11/21/18.
//

import Foundation

struct AdvertInfo: Codable {
    
    var adv_data: AdvertData?
}

public struct AdvertData: Codable {
    
    var url: String?
    var title: String?
    var short_info: String?
    var price_usd: String?
    var price: String?
    var main_photo_url: String?
    var is_deleted: Bool?
    var id: Int?
    var owner_name: String?
}

