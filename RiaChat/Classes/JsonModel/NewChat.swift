//
//  NewChat.swift
//  RiaChat
//
//  Created by Loyanich on 11/27/18.
//

import Foundation

struct NewChat: Codable {
    
    var chat: ChatListItem?
}
