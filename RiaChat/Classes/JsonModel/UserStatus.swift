//
//  UserStatus.swift
//  RiaChat
//
//  Created by Loyanich on 11/23/18.
//

import Foundation

struct UserStatus: Codable {
    
    var online: Int?
    var responce_time: Int?
}
