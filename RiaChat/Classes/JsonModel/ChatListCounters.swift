//
//  ChatListCounters.swift
//  RiaChat
//
//  Created by Loyanich on 11/6/18.
//

import Foundation

struct ChatListCounters: Codable {
    
    var active: Int
    var archive: Int
    var other: Int
    var spam: Int
}
