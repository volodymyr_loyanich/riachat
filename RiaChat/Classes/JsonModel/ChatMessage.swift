//
//  File.swift
//  RiaChat
//
//  Created by Loyanich on 11/14/18.
//

import Foundation

struct ChatMessages: Codable {
    
    var message_list: [ChatMessage]?
    var messages_left: Int?
}

struct ChatMessage: Codable {
    
    var time: String?
    var text: String?
    var status: String?
    var id: Int?
    var date: String?
    var author_id: Int?
}
