//
//  ChatListNewMessage.swift
//  RiaChat
//
//  Created by Loyanich on 11/9/18.
//

import Foundation

struct ChatListNewMessage: Codable {
    
    var message: ChatMessage?
    var chat_id: Int?
//    var unreaded_messages: Int
}

struct Message: Codable {
    
    var time: String?
    var text: String?
    var status: String?
    var id: Int?
    var date: String?
    var author_id: Int?
    //    var unreaded_messages: Int
}

//{"message":{"time":"14:54","text":"testfdsfds","status":"Отправлено","id":807,"date":"3 Апреля 2018 г.","author_id":5027249},"chat_id":1})
