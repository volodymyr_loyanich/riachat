//
//  UserInfo.swift
//  RiaChat
//
//  Created by Loyanich on 11/21/18.
//

import Foundation

struct UserInfo: Codable {
    
    var site_age: String?
    var name: String?
    var last_visit: String?
    var id: Int?
    var email: String?
    var bank_checked: Int?
    var avatar_url: String?
    var adv_count: Int?
    var adv_closed: Int?
    var phones: [UserPhoneResponse]?
}

struct UserPhoneResponse: Codable {
    
    var number: String?
    var id: Int?
    var checked: Int?
    var call_till: String?
    var call_name: String?
    var call_from: String?
}

