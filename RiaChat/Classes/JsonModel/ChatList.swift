//
//  ChatListCodable.swift
//  Alamofire
//
//  Created by Loyanich on 10/31/18.
//

import Foundation

struct ChatListItem: Codable {
    var id: Int?
    var owner_id: Int?
    var owner_name: String?
    var opponent_id: Int?
    var opponent_name: String?
    var text: String?
    var time: String?
    var date: String?
    var photo: String?
    var title: String?
    var unreaded_messages: Int?
    var author_id: Int?
    var project_id: Int?
    var is_deleted: Bool?
    var adv_id: Int?
    var updated_at: String?
    var nc_id: Int?
    var chat_id: Int?
}
