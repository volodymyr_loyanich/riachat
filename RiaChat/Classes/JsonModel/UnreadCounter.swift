//
//  UnreadCounter.swift
//  RiaChat
//
//  Created by Loyanich on 12/13/18.
//

import Foundation

struct UnreadCounter: Codable {
    
    var count: Int?
}
