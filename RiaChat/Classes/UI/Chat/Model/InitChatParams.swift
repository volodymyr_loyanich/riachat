//
//  InitChatParams.swift
//  RiaChat
//
//  Created by Loyanich on 11/14/18.
//

import Foundation

open class InitChatDesignParams {
    
    let navigationBarTintColor: UIColor
    let navigationBarTextTitleColor: UIColor
    let navigationBarTextOfflineColor: UIColor
    let navigationBarTextOnlineColor: UIColor
    let navigationBarBackgroundColor: UIColor
    let navigationBarPhoneImageName: String
    let navigationBarInfoImageName: String
    let navigationBarBackButtonImageName: String
    
    public init(navigationBarTintColor: UIColor, navigationBarTextTitleColor: UIColor, navigationBarTextOfflineColor: UIColor, navigationBarTextOnlineColor: UIColor, navigationBarBackgroundColor: UIColor, navigationBarPhoneImageName: String, navigationBarInfoImageName: String, navigationBarBackButtonImageName: String) {
        
        self.navigationBarTintColor = navigationBarTintColor
        self.navigationBarTextTitleColor = navigationBarTextTitleColor
        self.navigationBarTextOfflineColor = navigationBarTextOfflineColor
        self.navigationBarTextOnlineColor = navigationBarTextOnlineColor
        self.navigationBarBackgroundColor = navigationBarBackgroundColor
        self.navigationBarPhoneImageName = navigationBarPhoneImageName
        self.navigationBarInfoImageName = navigationBarInfoImageName
        self.navigationBarBackButtonImageName = navigationBarBackButtonImageName
    }
}


open class InitChatParams {
    
    public typealias ActionShowAdvert = (_ advertData: ChatInfo, _ viewController: UIViewController?) -> Void
    public typealias ActionSendAnalytics = (_ analyticsModel: AnalyticsModel) -> Void
    public typealias UpdateCounts = (_ changedCount: Int, _ newCount: Int?) -> Void
    
    let userID: Int
    let userAvatarUrl: String
    var chatID: Int
    let advertID: Int
    let secureKey: String
    let imagePlaceholderResName: String
    let projectID: Int
    let isAssistant: Bool
    let languageID: String
    let pspID: String
    let unreadedMessages: Int
    
    var showAdvertAction: ActionShowAdvert?
    var sendAnalytics: ActionSendAnalytics?
    var updateCounts: UpdateCounts?
    
    var initChatDesignParams: InitChatDesignParams?
    var initChatInfoParams: InitChatInfoParams?
    
    public init(userID: Int, unreadedMessages: Int, userAvatarUrl: String, chatID: Int, advertID: Int, secureKey: String, imagePlaceholderResName: String, projectID: Int, isAssistant: Bool, languageID: String, pspID: String, initChatDesignParams: InitChatDesignParams?, initChatInfoParams: InitChatInfoParams?, sendAnalytics: ActionSendAnalytics?, showAdvertAction: ActionShowAdvert?, updateCounts: UpdateCounts?) {

        self.userID = userID
        self.userAvatarUrl = userAvatarUrl
        self.chatID = chatID
        self.advertID = advertID
        self.secureKey = secureKey
        self.imagePlaceholderResName = imagePlaceholderResName
        self.projectID = projectID
        self.isAssistant = isAssistant
        self.languageID = languageID
        self.pspID = pspID
        
        self.showAdvertAction = showAdvertAction
        self.sendAnalytics = sendAnalytics
        self.updateCounts = updateCounts
        
        self.initChatDesignParams = initChatDesignParams
        self.initChatInfoParams = initChatInfoParams
        self.unreadedMessages = unreadedMessages
    }
}
