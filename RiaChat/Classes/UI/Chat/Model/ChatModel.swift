//
//  ChatModel.swift
//  RiaChat
//
//  Created by Loyanich on 11/14/18.
//

import Foundation
import RxDataSources

enum ChatCellType {
    case left
    case right
    case data
}

struct ChatModel {

    var time: String
    var text: String
    var status: String
    var id: Int
    var rawDate: String
    var dateCustomString: String
    var authorID: Int
    var messagebackgroundColor: UIColor
    var cellType: ChatCellType
    var avatarVisibility: Bool
    var avatarUrl: String
}

