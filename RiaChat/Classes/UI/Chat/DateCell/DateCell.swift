//
//  DateCell.swift
//  RiaChat
//
//  Created by Loyanich on 11/16/18.
//

import UIKit

class DateCell: UITableViewCell {
    
    @IBOutlet weak var messageDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.transform = CGAffineTransform(rotationAngle: (CGFloat)(Double.pi))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
