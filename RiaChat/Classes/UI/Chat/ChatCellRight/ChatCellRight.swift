//
//  ChatCell.swift
//  RiaChat
//
//  Created by Loyanich on 11/13/18.
//

import UIKit

class ChatCellRight: UITableViewCell {
    
    @IBOutlet weak var message: UITextView!
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var messageIndicator: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        messageView.layer.cornerRadius = 3
        avatar.layer.cornerRadius = avatar.frame.height/2
        avatar.layer.masksToBounds = true
        self.transform = CGAffineTransform(rotationAngle: (CGFloat)(Double.pi))
//        self.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
