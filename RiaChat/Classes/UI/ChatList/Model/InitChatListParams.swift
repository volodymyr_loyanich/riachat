//
//  InitChatListParams.swift
//  RiaChat
//
//  Created by Loyanich on 11/27/18.
//

import Foundation

open class InitChatListDesignParams {
    
    let navigationBarTintColor: UIColor
    let navigationBarTextColor: UIColor
    let navigationBarbackgroundColor: UIColor
    let navigationBarImageName: String
    
    public init(navigationBarTintColor: UIColor, navigationBarTextColor: UIColor, navigationBarbackgroundColor: UIColor, navigationBarImageName: String) {
        
        self.navigationBarTintColor = navigationBarTintColor
        self.navigationBarTextColor = navigationBarTextColor
        self.navigationBarbackgroundColor = navigationBarbackgroundColor
        self.navigationBarImageName = navigationBarImageName
    }
}

open class InitChatListParams {
    
    public typealias ActionShowAdvert = (_ advertData: ChatInfo, _ viewController: UIViewController?) -> Void
    public typealias ActionSendAnalytics = (_ analyticsModel: AnalyticsModel) -> Void
    public typealias UpdateCounts = (_ changedCount: Int, _ newCount: Int?) -> Void
    
    let userID: Int
    let userAvatarUrl: String
    let languageID: String
    let pspID: String
    var initChatListDesignParams: InitChatListDesignParams?
    var initChatDesignParams: InitChatDesignParams?
    var initChatInfoParams: InitChatInfoParams?
    var showAdvertAction: ActionShowAdvert?
    var sendAnalytics: ActionSendAnalytics?
    var updateCounts: UpdateCounts?

//        = "2"
//    let userID = "929064"
    
    public init(userID: Int, userAvatarUrl: String, languageID: String, pspID: String, initChatListDesignParams: InitChatListDesignParams?, initChatDesignParams: InitChatDesignParams?, initChatInfoParams: InitChatInfoParams?, sendAnalytics: ActionSendAnalytics?, showAdvertAction: ActionShowAdvert?, updateCounts: UpdateCounts?) {
        
        self.userID = userID
        self.userAvatarUrl = userAvatarUrl
        self.languageID = languageID
        self.pspID = pspID
        self.initChatListDesignParams = initChatListDesignParams
        self.initChatDesignParams = initChatDesignParams
        self.initChatInfoParams = initChatInfoParams
        self.showAdvertAction = showAdvertAction
        self.sendAnalytics = sendAnalytics
        self.updateCounts = updateCounts
    }
}
