//
//  ChatListModel.swift
//  Alamofire
//
//  Created by Loyanich on 11/1/18.
//

import Foundation
//struct ChatModel: Codable {
struct ChatListModel {
    
    let chatID: Int
    let imageUrl: String
    let title: String
    let message: NSMutableAttributedString
    let time: String
    let date: String
    let unreadedMessages: Int
    let imagePlaceholderResName: String
    let isDeleted: Bool
    var advertID: Int
    var projectID: Int
    var ownerID: Int
    
//    init(chatID: Int, imageUrl: String, title: String, message: NSMutableAttributedString, time: String, date: String, unreadedMessages: Int,
//         imagePlaceholderResName: String, isDeleted: Bool, advertID: Int) {
//
//        self.chatID = chatID
//        self.imageUrl = imageUrl
//        self.title = title
//        self.message = message
//        self.time = time
//        self.date = date
//        self.unreadedMessages = unreadedMessages
//        self.imagePlaceholderResName = imagePlaceholderResName
//        self.isDeleted = isDeleted
//        self.advertID = advertID
//    }
}
