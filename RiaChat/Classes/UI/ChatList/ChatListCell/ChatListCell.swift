//
//  ChatListCell.swift
//  Alamofire
//
//  Created by Loyanich on 10/30/18.
//

import UIKit

class ChatListCell: UITableViewCell{

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var unreadMessageCount: UILabel!
    @IBOutlet weak var newMessageView: UIView!
    @IBOutlet weak var cellMenu: UIButton!
    @IBOutlet weak var deletePlashkaView: UIView!
    @IBOutlet weak var deletePlashkaText: UILabel!
    
    var blurEffectView: UIVisualEffectView? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView?.frame = avatarImageView.bounds
        blurEffectView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView?.alpha = 0.5
        
        deletePlashkaView.layer.cornerRadius = 2
        deletePlashkaView.layer.masksToBounds = false
        
        newMessageView.layer.cornerRadius = 2
        newMessageView.layer.masksToBounds = false
        
        avatarImageView.layer.borderWidth = 1
        avatarImageView.layer.borderColor = UIColor.ria_gray_E5E5E5.cgColor
        
        if let blurEffectView = blurEffectView {
            
            avatarImageView.addSubview(blurEffectView)
        }
    }
//    var model: ChatListModel? {
//        didSet {
//            updateViews()
//        }
//    }
    
//
//    func updateViews() -> <#return type#> {
//
//        guard  let model = model as? ButtonCellModel else {
//            return
//        }
//
//        button.setTitle(model.title, for: .normal)
//    }
}
