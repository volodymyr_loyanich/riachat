//
//  ChatInfo.swift
//  RiaChat
//
//  Created by Loyanich on 11/21/18.
//

import Foundation

open class ChatInfo {
    
    public var id: Int
    public var url: String
    public var projectId: Int
    
    public init(id: Int, url: String, projectId: Int) {
        
        self.id = id
        self.url = url
        self.projectId = projectId
    }
}

