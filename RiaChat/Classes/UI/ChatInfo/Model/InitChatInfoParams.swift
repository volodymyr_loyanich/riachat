//
//  InitChatInfoParams.swift
//  RiaChat
//
//  Created by Vladimir Loyanich on 29/11/2018.
//

import Foundation

//InitChatInfoDesignParams
//InitChatInfoParams
open class InitChatInfoDesignParams {
    
    let navigationBarBackgroundColor: UIColor
    let navigationBarBackButtonImageName: String
    
    public init(navigationBarBackgroundColor: UIColor, navigationBarBackButtonImageName: String){
        
        self.navigationBarBackgroundColor = navigationBarBackgroundColor
        self.navigationBarBackButtonImageName = navigationBarBackButtonImageName
    }
}

open class InitChatInfoParams {
    
    var initChatInfoDesignParams: InitChatInfoDesignParams?
    
    public init(initChatInfoDesignParams: InitChatInfoDesignParams?){
        
        self.initChatInfoDesignParams = initChatInfoDesignParams
    }
    
}
