//
//  ChatInfoController.swift
//  RiaChat
//
//  Created by Loyanich on 11/21/18.
//

import UIKit
import SnapKit

class ChatInfoController: UIViewController {

    @IBOutlet  var image: UIImageView!
    @IBOutlet  var advertTitle: UILabel!
    @IBOutlet  var advertLocation: UILabel!
    @IBOutlet  var price: UILabel!
    @IBOutlet  var advertLink: UIButton!
    @IBOutlet  var avatar: UIImageView!
    @IBOutlet  var authorName: UILabel!
    @IBOutlet  var lastVisit: UILabel!
    @IBOutlet weak var deletePlashkaView: UIView!
    @IBOutlet weak var deletePlashkaText: UILabel!
    let assetsBundle = Bundle(for: AssetsBundle.self)
    var lastView: UIView?
    
    var advertData: AdvertData?
    var userInfo: UserInfo?
    var initChatParams: InitChatParams?
    var selectedPhoneNamber: String?
    
    deinit {
        print("deinit ChatInfoController")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let navigationBarbackgroundColor = initChatParams?.initChatDesignParams?.navigationBarBackgroundColor ?? UIColor.ria_black_414042
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.backgroundColor = navigationBarbackgroundColor
        
        image.layer.borderWidth = 0.1
        image.layer.borderColor = UIColor.ria_image_stroke.cgColor
        image.clipsToBounds = true
        // Do any additional setup after loading the view.
        
        let url = URL(string: advertData?.main_photo_url ?? "")
        let imagePlaceholder = UIImage(named: initChatParams?.imagePlaceholderResName ?? "no_image_ria", in: assetsBundle, compatibleWith: nil)
        image.backgroundColor = UIColor.clear
        image.kf.setImage(with: url, placeholder: imagePlaceholder)
        
        advertTitle.text = advertData?.title
        deletePlashkaText.text = localizedString(forKey: "deletePlashkaText")
        
        if let isDeleted = advertData?.is_deleted, isDeleted {
            
            deletePlashkaView.isHidden = false
            advertTitle.textColor = UIColor.ria_gray_9B9B9B
            addBlurEffectOnView(view: image)
            price.isHidden = true
            advertLocation.isHidden = true
        } else {
            
            deletePlashkaView.isHidden = true
            advertTitle.textColor = UIColor.ria_blue_256799
            price.isHidden = false
            advertLocation.isHidden = false
            price.attributedText = makeAttrPrice()
        }
        
        lastVisit.text = localizedString(forKey: "lastActivity") + ": " + (userInfo?.last_visit ?? "")
        advertLink.setTitle(localizedString(forKey: "watchOnSite"), for: .normal)
        authorName.text = userInfo?.name
        
        let avatarUrl = URL(string: userInfo?.avatar_url ?? "")
        let avatarImagePlaceholder = UIImage(named: "chatInfoAvatar", in: assetsBundle, compatibleWith: nil)
        avatar.backgroundColor = UIColor.clear
        avatar.kf.setImage(with: avatarUrl, placeholder: avatarImagePlaceholder)
        avatar.layer.cornerRadius = avatar.frame.height/2
        avatar.layer.masksToBounds = true
        
        if let timeOnSite = userInfo?.site_age {
            
            addWorkWithSite(timeOnSite: timeOnSite)
        }
        
        if let phones = userInfo?.phones {
            
            var showTitle = false
            for phone in phones {
                
                if let phoneNumber = phone.number, !phoneNumber.isEmpty {
                    
                    if !showTitle,
                        let checked = phone.checked,
                        checked == 1 {
                        
                        showTitle = true
                    }
                }
            }
            
            if showTitle {
                
                addCheckPhoneTitle()
            }
            
            for phone in phones {
                
                if let phoneNumber = phone.number, !phoneNumber.isEmpty {
                    
                    var phoneColor = UIColor.ria_gray_9B9B9B
                    if let checked = phone.checked,
                        checked == 1 {
                        
                        phoneColor = UIColor.ria_phone_namber
                    }
                    
                    addPhoneNamber(phoneNamber: phoneNumber, phoneColor: phoneColor)
                }
            }
        }
    }
    
    @IBAction func showAdvertInfo(_ sender: UIButton) {
        
        let chatInfo = ChatInfo.init(id: advertData?.id ?? 0, url: advertData?.url ?? "", projectId: initChatParams?.projectID ?? 0)
        
        initChatParams?.showAdvertAction?(chatInfo, self)
    }
    
    func makeAttrPrice() -> NSAttributedString {
        
        var priceUAH = ""
        var priceUSD = ""

        var separator = ""
        
        if let priceUAHRaw = advertData?.price {
            
            priceUAH = Formatter.withSeparator.string(for: Int(priceUAHRaw)) ?? ""
            
            if priceUAH != "" {
                priceUAH = priceUAH + " грн"
            }
        }
        
        if let priceUSDRaw = advertData?.price_usd {
            
            priceUSD = Formatter.withSeparator.string(for: Int(priceUSDRaw)) ?? ""
            
            if priceUSD != "" {
                priceUSD = priceUSD + " $"
            }
        }
        
        if priceUAH != "", priceUSD != ""{

            separator = " /"
        }

        let attributes = [(text: "\(priceUSD)", color: UIColor.ria_darck_green, font: UIFont.Arial.bold(21)),
                          (text: "\(separator) \(priceUAH)", color: UIColor.ria_black_414042, font: UIFont.Arial.regular(15))]

        let attributed_string = NSMutableAttributedString.setText(attributes)

        return attributed_string
    }

    func addWorkWithSite(timeOnSite: String){
        
        func setTitle(container: UIView) -> UILabel {
            
            var titleText: String?
            
            if let projectID = initChatParams?.projectID {
                
                switch projectID {
                case 2:
                    
                    titleText = localizedString(forKey: "workWithDOM")
                case 3:

                    titleText = localizedString(forKey: "workWithAUTO")
                default:
                    
                    titleText = localizedString(forKey: "workWithRIA")
                }
            }
            
            let title = UILabel()
            title.text = titleText
            title.font = UIFont.Arial.regular(15)
            title.textColor = UIColor.ria_black_414042

            container.addSubview(title)
            return title
        }

        func serWorkTime(container: UIView) -> UILabel {
            
            let time = UILabel()
            time.text = timeOnSite
            //"6 лет 2 месяца"
            time.font = UIFont.Arial.bold(15)
            time.textColor = UIColor.black
            
            container.addSubview(time)
            return time
        }
        
        let container = UIView()
        self.view.addSubview(container)
        self.lastView = container
        
        let checkIcon = checkIconImage(container: container)
        let title = setTitle(container: container)
        let time = serWorkTime(container: container)
        
        container.snp.makeConstraints {
            
            $0.top.equalTo(avatar.snp.bottom).offset(16)
            $0.leading.equalTo(13)
            $0.trailing.equalTo(-13)
            $0.height.greaterThanOrEqualTo(20)
        }
        
        checkIcon.snp.makeConstraints {
            
            $0.leading.equalTo(0)
            $0.centerY.equalTo(container.snp.centerY)
        }
        
        title.snp.makeConstraints {
            
            $0.leading.equalTo(checkIcon.snp.trailing).offset(4)
            $0.centerY.equalTo(container.snp.centerY)
        }
        
        time.snp.makeConstraints {
            
            $0.leading.equalTo(title.snp.trailing).offset(4)
            $0.centerY.equalTo(container.snp.centerY)
        }
    }
    
    func addCheckPhoneTitle(){
        
        func setTitle(container: UIView) -> UILabel {
            
            let title = UILabel()
            title.text = localizedString(forKey: "checkedPhones")
            title.font = UIFont.Arial.regular(15)
            title.textColor = UIColor.ria_black_414042
            
            container.addSubview(title)
            return title
        }
        
        let container = UIView()
        self.view.addSubview(container)

        
        let checkIcon = checkIconImage(container: container)
        let title = setTitle(container: container)
        
        let lastView: UIView = self.lastView ?? avatar
        let lastViewOffset = self.lastView != avatar ? 5 : 16
        
        container.snp.makeConstraints {
            
            $0.top.equalTo(lastView.snp.bottom).offset(lastViewOffset)
            $0.leading.equalTo(13)
            $0.trailing.equalTo(-13)
            $0.height.greaterThanOrEqualTo(20)
        }
        
        checkIcon.snp.makeConstraints {
            
            $0.leading.equalTo(0)
            $0.centerY.equalTo(container.snp.centerY)
        }
        
        title.snp.makeConstraints {
            
            $0.leading.equalTo(checkIcon.snp.trailing).offset(4)
            $0.centerY.equalTo(container.snp.centerY)
        }
        
        self.lastView = container
    }
    
    func addPhoneNamber(phoneNamber: String, phoneColor: UIColor){
        
        let phone = Button()
        phone.setTitle(phoneNamber,for: .normal)
        phone.setTitleColor(phoneColor, for: .normal)
        phone.titleLabel?.font = UIFont.Arial.bold(20)
        phone.contentHorizontalAlignment = .left
        
        phone.setAction {
            
            var numberStr = phoneNamber
            
            for substr in [" ", "/", "-", "(", ")"] {
                
                numberStr = numberStr.replacingOccurrences(of: substr, with: "", options: .literal, range: nil)
            }
            
            if let phonePath = URL(string: "tel:" + numberStr){
                
                UIApplication.shared.openURL(phonePath)
                
                if let initChatParams = self.initChatParams,
                    let userInfo = self.userInfo,
                    let advertData = self.advertData {
                    
                    SendAnalytics.phoneCall(initChatParams: initChatParams, userInfo: userInfo, advertData: advertData, chatPlaceId: "3")
                }
            }
        }
        
        self.view.addSubview(phone)
        
        let lastView: UIView = self.lastView ?? avatar
        
        phone.snp.makeConstraints {
            
            $0.top.equalTo(lastView.snp.bottom)
            $0.leading.equalTo(35)
            $0.trailing.equalTo(-16)
            $0.height.equalTo(40)
        }
        
        self.lastView = phone
    }
    
    func checkIconImage(container: UIView) -> UIImageView {
        
        let image: UIImage? = UIImage(named: "i16_check", in: assetsBundle, compatibleWith: nil)
        let checkIcon = UIImageView.init(image: image)
        checkIcon.contentMode = .scaleAspectFit
        
        container.addSubview(checkIcon)
        return checkIcon
        
    }
    
    func addBlurEffectOnView(view: UIView) {
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.alpha = 0.5
        view.addSubview(blurEffectView)
    }
}
