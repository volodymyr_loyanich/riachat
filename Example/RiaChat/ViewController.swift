//
//  ViewController.swift
//  RiaChat
//
//  Created by Vladimir on 10/29/2018.
//  Copyright (c) 2018 Vladimir. All rights reserved.
//

import UIKit
import RiaChat

class ViewController: UIViewController {

    @IBAction func press(_ sender: Any) {

        
        //                            "secureKey": "765d61d954d65f8f975971112b426f12"
        let userID = 929064
        let chatID = 0
        let secureKey = "765d61d954d65f8f975971112b426f12"
        //                            let advertID = 22905897
        let advertID = 22905897
        let imagePlaceholderResName = "no_image_auto"
        let projectID = 3
        let isAssistant = false

        let pspID = "437c8907ea10b91e6ed6bd63a25e49496d1b5d7924f440e985e1c5d03f393c5c929064"

        let chatNavigationBarTintColor = UIColor.ria_brand_color_pale_red
        let navigationBarTextTitleColor = UIColor.white
        let navigationBarTextOfflineColor = UIColor.white
        let navigationBarTextOnlineColor = UIColor.white
        let navigationBarPhoneImageName = "phoneIcon"
        let navigationBarInfoImageName = "infoIcon"
        let navigationBarBackButtonImageName = "backNaviArrow"
        let chatNavigationBarBackgroundColor = UIColor.ria_brand_color_pale_red
        
        let initChatDesignParams = InitChatDesignParams(navigationBarTintColor: chatNavigationBarTintColor, navigationBarTextTitleColor: navigationBarTextTitleColor, navigationBarTextOfflineColor: navigationBarTextOfflineColor, navigationBarTextOnlineColor: navigationBarTextOnlineColor, navigationBarBackgroundColor: chatNavigationBarBackgroundColor, navigationBarPhoneImageName: navigationBarPhoneImageName, navigationBarInfoImageName: navigationBarInfoImageName, navigationBarBackButtonImageName: navigationBarBackButtonImageName)
        
        let initChatParams = InitChatParams(userID: userID, unreadedMessages: 0, userAvatarUrl: "", chatID: chatID, advertID: advertID, secureKey: secureKey, imagePlaceholderResName: imagePlaceholderResName, projectID: projectID, isAssistant: isAssistant, languageID: "2", pspID: pspID, initChatDesignParams: initChatDesignParams, initChatInfoParams: nil, sendAnalytics: nil, showAdvertAction: nil, updateCounts: nil)
        
        //                            NavigationHelper.goTo(self_, taget_vc_name: "AdFeedback", params: p())
        let chatVC = Fasad.openChat(initChatParams: initChatParams)
        
        self.navigationController?.pushViewController(chatVC, animated: true)
        //self.present(vc, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        let userID = 929064
        let languageID = "2"
        let navigationBarTintColor = UIColor.ria_brand_color_pale_red
        let navigationBarbackgroundColor = UIColor.ria_brand_color_pale_red
        let navigationBarTextColor = UIColor.white
        let navigationBarImageName = "arrow_to_down"
        
        let chatNavigationBarTintColor = UIColor.ria_brand_color_pale_red
        let navigationBarTextTitleColor = UIColor.white
        let navigationBarTextOfflineColor = UIColor.white
        let navigationBarTextOnlineColor = UIColor.white
        let navigationBarPhoneImageName = "phoneIcon"
        let navigationBarInfoImageName = "infoIcon"
        let navigationBarBackButtonImageName = "backNaviArrow"
        let chatNavigationBarBackgroundColor = UIColor.ria_brand_color_pale_red
        
        let infoNavigationBarbackgroundColor = UIColor.ria_brand_color_pale_red
        
        let initChatInfoDesignParams = InitChatInfoDesignParams(navigationBarBackgroundColor: chatNavigationBarBackgroundColor, navigationBarBackButtonImageName: navigationBarBackButtonImageName)
        let initChatInfoParams = InitChatInfoParams(initChatInfoDesignParams: initChatInfoDesignParams)
        
        let initChatDesignParams = InitChatDesignParams(navigationBarTintColor: chatNavigationBarTintColor, navigationBarTextTitleColor: navigationBarTextTitleColor, navigationBarTextOfflineColor: navigationBarTextOfflineColor, navigationBarTextOnlineColor: navigationBarTextOnlineColor, navigationBarBackgroundColor: chatNavigationBarBackgroundColor, navigationBarPhoneImageName: navigationBarPhoneImageName, navigationBarInfoImageName: navigationBarInfoImageName, navigationBarBackButtonImageName: navigationBarBackButtonImageName)
        
        let initChatListDesignParams = InitChatListDesignParams(navigationBarTintColor: navigationBarTintColor, navigationBarTextColor: navigationBarTextColor, navigationBarbackgroundColor: navigationBarbackgroundColor, navigationBarImageName: navigationBarImageName)
        
        let initChatListParams = InitChatListParams(userID: userID, userAvatarUrl: "https://cdn.riastatic.com/photos/avatars/all/92/9290/929064/929064b.jpg?v=1543409815", languageID: languageID, pspID: "437c8907ea10b91e6ed6bd63a25e49496d1b5d7924f440e985e1c5d03f393c5c929064", initChatListDesignParams: initChatListDesignParams, initChatDesignParams: initChatDesignParams, initChatInfoParams: nil, sendAnalytics: nil, showAdvertAction: { (advertData, vc) in
                print("adadxaxaxa")
            },updateCounts: { (changedCount, newCounts) in
                print(changedCount)
                print(newCounts)
            })
        
//        Fasad.openChatList(parentViewController: self)
        self.navigationController?.view.backgroundColor = UIColor.white
        let vc = Fasad.openChatList(initChatListParams: initChatListParams)
        
        self.navigationController?.pushViewController(vc, animated: true)
//        let vc = ChatListRouter.createModule()
//        self.navigationController?.pushViewController(vc, animated: true)
        // Do any additional setup after loading the view, typically from a nib.
    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }

}

