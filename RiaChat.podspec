#
# Be sure to run `pod lib lint RiaChat.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'RiaChat'
  s.version          = '0.0.2'
  s.summary          = 'A short description of RiaChat.'
  s.platform         = :ios, '9.0'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://git.ria.com/d_yavorskiy/riachat'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Vladimir' => 'volodymyr.loyanich@ria.com' }
  s.source           = { :git => 'https://git.ria.com/d_yavorskiy/riachat.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  # s.source_files = 'RiaChat/**/*.{h,m,swift,xib,strings}'
   s.source_files = 'RiaChat/**/*.{xib,swift,strings,png}'
#  s.source_files = 'RiaChat/**/*'
#s.source_files = 'RiaChat/*'

  s.resource_bundles = {
#      'RiaChat' => ['RiaChat/RiaChat/**/*.swift/*.{xib,swift,strings}']
#      'RiaChat' => ['RiaChat/**/*.{xib,swift,strings}']
#        'RiaChat' => ['RiaChat/*']
#      'RiaChat' => ['RiaChat/Classes/ChatLocalizable/*.bundle']
      'RiaChat' => ['RiaChat/**']
#        'RiaChat' => ['RiaChat/Classes/ChatLocalizable/*.{bundle,strings}']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'ЛЛЛл
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'

    s.dependency 'Kingfisher'
    s.dependency 'Moya/RxSwift'
    s.dependency 'RxSwift'
    s.dependency 'RxCocoa'
    s.dependency 'SnapKit'
    s.dependency 'RxDataSources'
    s.dependency 'SwiftPhoenixClient'
    s.dependency 'Localize-Swift'
    s.dependency 'IQKeyboardManagerSwift'
end
